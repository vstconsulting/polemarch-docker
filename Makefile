# Config deployment
NAMESPACE_PM = polemarch
#NAMESPACE_PM - NAMESPACE_PM leave the variable empty if you want to set the default NAMESPACE
REPLICA_VOL = 3
TAG_PM = latest
IMAGE_PM = 'registry.gitlab.com\/vstconsulting\/polemarch-docker'
DOMAIN_NAME = example.com
TIMEZONE_PM = 'Asia\/Vladivostok'
POLEMARCH_DEBUG_PM = true
# POLEMARCH_DEBUG_PM = true or false
POLEMARCH_LOG_LEVEL_PM = DEBUG
# POLEMARCH_LOG_LEVEL_PM = WARNING or DEBUG
STORAGE_SQL = 5Gi
# STORAGE_SQL = default 5Gi
STORAGE_PM = $(STORAGE_SQL)
# STORAGE_PM = default from STORAGE_SQL


# Variables
TEMPLATE_FILE_YAML=./kube_pm_tamplate.yaml
# KUBE_YAML - the variable in which the values from config are entered
KUBE_YAML = cat $(TEMPLATE_FILE_YAML) | \
		sed s/__IMAGE_PM__/$(IMAGE_PM)/g | \
		sed s/__TAG_PM__/$(TAG_PM)/g | \
		sed s/__REPLICA_VOL__/$(REPLICA_VOL)/g | \
		sed s/__DOMAIN_NAME__/$(DOMAIN_NAME)/g | \
		sed s/__TIMEZONE_PM__/$(TIMEZONE_PM)/g | \
		sed s/__POLEMARCH_DEBUG_PM__/$(POLEMARCH_DEBUG_PM)/g | \
		sed s/__POLEMARCH_LOG_LEVEL_PM__/$(POLEMARCH_LOG_LEVEL_PM)/g | \
		sed s/__STORAGE_SQL__/$(STORAGE_SQL)/g | \
		sed s/__STORAGE_PM__/$(STORAGE_PM)/g


# Check variable namespace for the commond kubectl
ifdef NAMESPACE_PM
	CMD_NAMESPACE_PM = --namespace=$(NAMESPACE_PM)
endif


check:
	$(KUBE_YAML)

deploy:
	$(KUBE_YAML) | kubectl apply $(CMD_NAMESPACE_PM) -f -

delete:
	$(KUBE_YAML) | kubectl $(CMD_NAMESPACE_PM) delete -f -
